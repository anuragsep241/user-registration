import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserData } from './registration.model';
import { RegistrationService } from './services/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent {
  constructor(private registrationService: RegistrationService, private router: Router) {
  }

  /**
   * when user submits registration form register user in to the systemm.
   * @param user user data to be registered
   */
  registerUser(user: UserData){
    this.registrationService.registerUser(user).subscribe(() => {
      this.registrationService.isUserLoggedin.next(true);
      this.router.navigate(['/home']);
    });
  }
}
