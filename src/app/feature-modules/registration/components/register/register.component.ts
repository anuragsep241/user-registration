import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { UserData } from '../../registration.model';
import { pwdNameValidator } from '../../../../shared/registration.util';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  public registrationForm: FormGroup;
  public isSubmitted = false;
  @Output() registerUser = new EventEmitter<UserData>();

  constructor(private fb: FormBuilder) {
    this.registrationForm = this.fb.group(
      {
        firstName: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ]),
        lastName: new FormControl('', [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ]),
        email: new FormControl(''),
        password: new FormControl(''),
      },
      {
        validators: [pwdNameValidator],
      }
    );
  }

  /**
   * Get all the form control for registrationForm
   * @readonly
   * @memberof RegisterComponent
   */
  get f() {
    return this.registrationForm.controls;
  }

  /**
   * Submit form, check for form valid.
   * If form is valid emit the register user event.
   */
  public submit() {
    this.isSubmitted = true;
    this.registrationForm.markAsDirty();
    if (!this.registrationForm.valid) {
      return;
    }
    const { password, ...rest } = this.registrationForm.value;
    this.registerUser.emit(rest as UserData);
  }
}
