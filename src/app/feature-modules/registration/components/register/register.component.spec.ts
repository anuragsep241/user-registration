import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationComponent } from '../../registration.component';
import { EmailComponent } from '../email/email.component';
import { PasswordComponent } from '../password/password.component';

import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [
        RegistrationComponent,
        RegisterComponent,
        EmailComponent,
        PasswordComponent,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should emit registerUser on form submit', () => {
    spyOn(component.registerUser, 'emit');
    component.registrationForm.setValue({
      firstName: 'fName',
      lastName: 'lName',
      email: 'email@email.cc',
      password: 'Testingtesting',
    });
    component.submit();
    expect(component.f.firstName.value).toBe('fName');
    expect(component.registerUser.emit).toHaveBeenCalled();
  });

  it('should not emit registerUser on form submit when form is invalid', () => {
    spyOn(component.registerUser, 'emit');
    component.registrationForm.setValue({
      firstName: '',
      lastName: 'lName',
      email: 'email@email.cc',
      password: 'fNamegtesting',
    });
    component.submit();
    expect(component.registerUser.emit).not.toHaveBeenCalled();
  });
});
