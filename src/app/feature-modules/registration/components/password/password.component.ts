import { Component, forwardRef, OnInit } from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validators,
} from '@angular/forms';
import { passwordStrengthValidator } from '../../../../shared/registration.util';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: PasswordComponent,
      multi: true
    }
  ],
})
export class PasswordComponent implements OnInit, ControlValueAccessor {
  public control: FormControl;
  constructor() {
    this.control = new FormControl('', [Validators.required, Validators.minLength(8), passwordStrengthValidator]);
  }

  ngOnInit() {
    this.control.valueChanges.subscribe(() => this.propagateChange());
  }

  /**
   * Function registered to propagate a change to the parent
   */
  public propagateChange: any = () => {};

  /**
   * Function registered to propagate touched to the parent
   */
  public propagateTouched: any = () => {};
  /**
   * ControlValueAccessor Interface Methods to be implemented
   */
  writeValue(obj: any): void {
    this.control.setValue(obj);
  }
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  validate(control: FormControl) {
    const isNotValid = !(this.control && this.control.valid);
    return isNotValid && this.control.errors;
  }
}
