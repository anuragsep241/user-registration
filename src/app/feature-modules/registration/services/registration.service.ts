import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { UserData } from '../registration.model';

@Injectable({
  providedIn: 'root',
})
export class RegistrationService {
  private apiUrl = environment.apiUrl;
  public isUserLoggedin =  new BehaviorSubject<boolean>(false);
  constructor(private http: HttpClient) {}

  /**
   * register user in the system.
   * @param userData that to be sent in registration post request
   */
  registerUser(userData: UserData): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/users`, userData);
  }
}
