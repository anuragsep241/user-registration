import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { RegistrationService } from './registration.service';
import { UserData } from '../registration.model';


describe('RegistrationService', () => {
  let injector;
  let service: RegistrationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RegistrationService]
    });

    injector = getTestBed();
    service = injector.get(RegistrationService);
    httpMock = injector.get(HttpTestingController);
  });

  describe('#registerUser', () => {
    it('should return an Observable', () => {
      const user: UserData = {firstName: 'fName', lastName: 'lName', email: 'test@test.co'};

      service.registerUser(user).subscribe(data => {
        expect(data).toBeNull();
      });
    });
  });
});
