import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, getTestBed, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { EmailComponent } from './components/email/email.component';
import { PasswordComponent } from './components/password/password.component';
import { RegisterComponent } from './components/register/register.component';
import { RegistrationComponent } from './registration.component';
import { UserData } from './registration.model';
import { RegistrationService } from './services/registration.service';

class MockregistrationSerivce {
  isUserLoggedin = new BehaviorSubject<boolean>(false);
  registerUser = (user: UserData): Observable<any> => {
    return of({});
  }
}
const routerSpy = { navigate: jasmine.createSpy('navigate') };
describe('RegistrationComponent', () => {
  let fixture;
  let component;
  let injector;
  let service: RegistrationService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        FormsModule,
      ],
      declarations: [
        RegistrationComponent,
        RegisterComponent,
        EmailComponent,
        PasswordComponent,
      ],
      providers: [
        { provide: RegistrationService, useClass: MockregistrationSerivce },
        { provide: Router, useValue: routerSpy },
      ],
    }).compileComponents();
    injector = getTestBed();
    service = TestBed.inject(RegistrationService);
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should navigate to home when user is registered`, () => {
    const mySpy = spyOn(service, 'registerUser').and.returnValue(of({}));
    component.registerUser({
      firstName: 'test',
      lastName: 'lName',
      email: 'email@em.cc',
    });
    expect(mySpy).toHaveBeenCalledTimes(1);
    expect(service.isUserLoggedin.value).toBeTruthy();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/home']);
  });
});
