import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { RegistrationRoutingModule } from './registration-routing.module';
import { RegistrationComponent } from './registration.component';
import { RegisterComponent } from './components/register/register.component';
import { EmailComponent } from './components/email/email.component';
import { PasswordComponent } from './components/password/password.component';


@NgModule({
  declarations: [RegistrationComponent, RegisterComponent, EmailComponent, PasswordComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RegistrationRoutingModule
  ]
})
export class RegistrationModule { }
