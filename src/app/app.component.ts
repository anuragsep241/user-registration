import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationService } from './feature-modules/registration/services/registration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'user-registration';
  constructor(private registrationService: RegistrationService, private router: Router){
    if (!this.registrationService.isUserLoggedin.value){
      this.router.navigate(['/register']);
    }
  }
}
