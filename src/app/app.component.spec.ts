import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, getTestBed, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject } from 'rxjs';
import { AppComponent } from './app.component';
import { RegistrationService } from './feature-modules/registration/services/registration.service';

class MockregistrationSerivce {
  isUserLoggedin = new BehaviorSubject<boolean>(false);
}
const routerSpy = { navigate: jasmine.createSpy('navigate') };
describe('AppComponent', () => {
  let fixture;
  let component;
  let injector;
  let service: RegistrationService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [AppComponent],
      providers: [
        { provide: RegistrationService, useClass: MockregistrationSerivce },
        { provide: Router, useValue: routerSpy },
      ],
    }).compileComponents();
    injector = getTestBed();
    service = injector.get(RegistrationService);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'user-registration'`, () => {
    expect(component.title).toEqual('user-registration');
  });

  it(`should navigate to register when user is not logged in`, () => {
    expect(routerSpy.navigate).toHaveBeenCalledWith(['/register']);
  });
});
