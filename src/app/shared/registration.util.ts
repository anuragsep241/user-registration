import {
  AbstractControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

/**
 * Validator function to check the upper and lower case letters in password field
 * @param control for which this validation to be implemented
 */

export const passwordStrengthValidator: ValidatorFn = (
  control: AbstractControl
): ValidationErrors | null => {
  const value: string = control.value || '';
  const msg = '';
  if (!value) {
    return null;
  }

  const upperCaseCharacters = /[A-Z]+/g;
  const lowerCaseCharacters = /[a-z]+/g;
  if (
    upperCaseCharacters.test(value) === false ||
    lowerCaseCharacters.test(value) === false
  ) {
    return {
      passwordStrength:
        'Password must contain lowercase and uppercase letters.',
    };
  }
  return null;
};

/**
 * Validate password field if it has first name orr last m=name in it.
 * @param formgroup on which this validation is being implemented
 */

export const pwdNameValidator: ValidatorFn = (
  formgroup: FormGroup
): ValidationErrors | null => {
  if (!formgroup) {
    return null;
  }
  const pwdCtrlValue: string =
    formgroup.get('password') && (formgroup.get('password').value || '');
  const fNameCtrlValue: string =
    formgroup.get('firstName') && (formgroup.get('firstName').value || '');
  const lNameCtrlValue: string =
    formgroup.get('lastName') && (formgroup.get('lastName').value || '');
  if (
    fNameCtrlValue !== '' &&
    pwdCtrlValue !== '' &&
    pwdCtrlValue.toLowerCase().indexOf(fNameCtrlValue.toLowerCase()) > -1
  ) {
    return { pwdfName: ' Password must not contain first name.' };
  }

  if (
    lNameCtrlValue !== '' &&
    pwdCtrlValue !== '' &&
    pwdCtrlValue.toLowerCase().indexOf(lNameCtrlValue.toLowerCase()) > -1
  ) {
    return { pwdlName: ' Password must not contain last name.' };
  }
  return null;
};
