import { FormBuilder, FormControl } from '@angular/forms';
import {
  passwordStrengthValidator,
  pwdNameValidator,
} from './registration.util';

describe('passwordStrengthValidator', () => {
  const control = new FormControl('', [passwordStrengthValidator]);
  it(`should return invalid when control is having value 'testing'`, () => {
    control.setValue('testing');
    expect(control.valid).toBeFalse();
  });

  it(`should return invalid when control is having value 'TESTING'`, () => {
    control.setValue('TESTING');
    expect(control.valid).toBeFalse();
  });

  it(`should return valid when control is having value 'TESTINGtesting'`, () => {
    control.setValue('TESTINGtesting');
    expect(control.valid).toBeTruthy();
  });
});

describe('pwdNameValidator', () => {
  const fb: FormBuilder = new FormBuilder();
  const formGroup = fb.group(
    {
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      password: new FormControl(''),
    },
    {
      validators: [pwdNameValidator],
    }
  );

  it(`should return invalid when password is having firstName`, () => {
    formGroup.setValue({
      firstName: 'test',
      lastName: '',
      password: 'testttt',
    });
    expect(formGroup.valid).toBeFalse();
  });

  it(`should return invalid when password is having lastName`, () => {
    formGroup.setValue({
      firstName: '',
      lastName: 'test',
      password: 'testttt',
    });
    expect(formGroup.valid).toBeFalse();
  });

  it(`should return invalid when password is having firstName and  lastName`, () => {
    formGroup.setValue({
      firstName: 'test',
      lastName: 'fest',
      password: 'testfest',
    });
    expect(formGroup.valid).toBeFalse();
  });

  it(`should return valid when password is not having firstName and  lastName`, () => {
    formGroup.setValue({
      firstName: 'test',
      lastName: 'fest',
      password: 'njohirfugrhhjgjhl',
    });
    expect(formGroup.valid).toBeTruthy();
  });
});
