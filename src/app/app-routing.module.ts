import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/register', pathMatch: 'full' },
  {
    path: 'register',
    loadChildren: () =>
      import('./feature-modules/registration/registration.module').then(
        (m) => m.RegistrationModule
      ),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./feature-modules/dashboard/dashboard.module').then((m) => m.DashboardModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
