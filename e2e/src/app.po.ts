import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getElementText(name: string): Promise<string> {
    return element(by.name(name)).getText() as Promise<string>;
  }

  setElementText(name: string, text: string): Promise<void> {
    return element(by.name(name)).sendKeys(text) as Promise<void>;
  }

  submitForm(): Promise<void>{
    return element(by.name('submit-button-input')).click() as Promise<void>;
  }
}
