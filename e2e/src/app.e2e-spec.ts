import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getElementText('register-header')).toEqual('Register with us');
  });

  it('Should fill all the values and submit the form', () => {
    page.navigateTo();
    expect(page.getElementText('register-header')).toEqual('Register with us');
    page.setElementText('firstName', 'MyFirstName');
    page.setElementText('lastName', 'MyLastName');
    page.setElementText('email-input', 'myemail@email.co');
    page.setElementText('password-input', 'TestPassword');
    page.submitForm();
    expect(page.getElementText('welcome-header')).toEqual('Welcome !!');
  });

  it('Should not fill all the values and should not be able tosubmit the form', () => {
    page.navigateTo();
    expect(page.getElementText('register-header')).toEqual('Register with us');
    page.setElementText('firstName', '');
    page.setElementText('lastName', 'MyLastName');
    page.setElementText('email-input', 'myemail@email.co');
    page.setElementText('password-input', 'TestPassword');
    page.submitForm();
    expect(page.getElementText('register-header')).toEqual('Register with us');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
