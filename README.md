# UserRegistration

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.13.

## Install dependencies

Run `npm install` to install all the dependencies in `package.json`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further details

* User can register by giving first name, last name,email, password.
* Required validation is implemented for all input fields.
* `ControllValueAccessor` interface is used to create custom input fields.
* `PasswordComponent` and `EmailComponent` are createdd with above interface because both can have various validations and stand alone functionalities.
* All ther equired validation are implemented in a saperate shared util file.
* Unit test coverage is done for 100%.
* End to end test cases are created by covering negative and positive test scenarios.